module Type
  (Param : sig
     type (!'a, !'b) monfun
   end)
= struct
  open Param

  (* the type ['t poset] has elements that are values
     of type ['t] equipped with a partial order structure. *)
  type _ poset =
  | Linear : int -> int poset
    (* [Linear n] is the linearly-ordered lattice
       0 <= 1 <= 2 <= ... <= n-1
    *)

  | Product : 'lA poset * 'lB poset -> ('lA * 'lB) poset
    (* [Product(lA, lB) is the cartesian product poset.

       (a, b) : Product(lA, lB) iff a : lA and b : lB

       (a1, b1) <= (a2, b2) iff
         a1 <= a2 and b1 <= b2
    *)

  | Sum : 'lA poset * 'lB poset -> ('lA, 'lB) Either.t poset
    (* [Sum(lA, lB) is the disjoint union poset.

       (left a) : Sum(lA, lB) iff a : lA
       (right b) : Sum(lA, lB) iff b : lB

       (left a1) <= (left a2) iff a1 <= a2
       (right b1) <= (right b2) iff b1 <= b2

       Note: this is not a lattice, just a partial order,
       as [Sum(lA, lB)] does not have a minimal element.
    *)

  | MonFun : 'lA poset * 'lB poset -> ('lA, 'lB) monfun poset
    (* [MonFun(lA, lB)] is the poset of monotonous functions
       from poset [lA] to poset [lB].

       f : MonFun(lA,lB) iff
         f : lA -> lB
         and
         forall a1 <= a2 : lA, f(a1) <= f(a2) : lB

       f <= g : MonFun(lA, lB) iff
         forall a, f(a) <= g(a)
       or, equivalently
         forall a1 <= a2, f(a1) <= g(a2)
    *)

  type any_poset = Any : 'a poset -> any_poset
  type elem = Elem : 'a poset * 'a -> elem

  type (_, _) eq = Refl : ('a, 'a) eq

  let rec same_poset : type a b. a poset -> b poset -> (a, b) eq option =
    let ( let* ) = Option.bind in
    let ret (x : (a, b) eq) = Some x in
    fun lA lB -> match lA, lB with
      | Linear i, Linear j -> if i = j then Some Refl else None
      | Linear _, _ | _, Linear _ -> None
      | Product (a, b), Product (a', b') ->
        let* Refl = same_poset a a' in
        let* Refl = same_poset b b' in
        ret Refl
      | Product _, _ | _, Product _ -> None
      | Sum (a, b), Sum (a', b') ->
        let* Refl = same_poset a a' in
        let* Refl = same_poset b b' in
        ret Refl
      | Sum _, _ | _, Sum _ -> None
      | MonFun (a, b), MonFun (a', b') ->
        let* Refl = same_poset a a' in
        let* Refl = same_poset b b' in
        ret Refl
end

module Ord = struct
  type t = Lt | Eq | Gt

  let conj : t -> t -> t option =
    fun o1 o2 -> match o1, o2 with
      | Lt, Lt | Lt, Eq | Eq, Lt -> Some Lt
      | Eq, Eq -> Some Eq
      | Gt, Gt | Gt, Eq | Eq, Gt -> Some Gt
      | Lt, Gt | Gt, Lt -> None

  let leq : t option -> bool = function
    | Some (Lt | Eq) -> true
    | None | Some Gt -> false
end

module Naive
    (Config : sig
       val check_graph_order : bool
       val basic_ops : int ref
       val memoize_enumerate : bool
     end) =
 struct
  type ('a, 'b) function_graph = ('a * 'b) list

  include Type(struct
      type ('a, 'b) monfun = ('a, 'b) function_graph
    end)

  type memo_slot = Memo : 'a poset * 'a list -> memo_slot
  let memo_table : (any_poset, memo_slot) Hashtbl.t =
    Hashtbl.create 42

  let rec enumerate : type a . a poset -> a list =
    fun (type a) (poset : a poset) ->
      if not Config.memoize_enumerate
      then enumerate_ poset
      else begin
        match Hashtbl.find memo_table (Any poset) with
        | exception Not_found ->
          let enum = enumerate_ poset in
          Hashtbl.replace memo_table
            (Any poset) (Memo (poset, enum));
          enum
        | Memo (poset', enum) ->
          let Refl = Option.get (same_poset poset poset') in
          enum
      end

  and enumerate_ : type a . a poset -> a list =
    function
    | Linear n -> List.init n (fun i -> i)
    | Product (lA, lB) ->
      enumerate lA |> List.concat_map @@ fun a ->
      enumerate lB |> List.map @@ fun b ->
      (a, b)
    | Sum (lA, lB) ->
      List.map Either.left (enumerate lA)
      @
      List.map Either.right (enumerate lB)
    | MonFun (lA, lB) ->
      (* very naive approach: enumerate all functions,
         then filter the non-monotonous ones *)
      let domA = enumerate lA in
      let elems_B = enumerate lB in
      (* [partial dom] returns the list of all monotonous functions
         (as input-output graphs, that is, association lists)
         on a subset [dom] of [elems_A]. *)
      let rec partial = function
        | [] -> [[]]
        | a :: rest_domA ->
          let funs_on_rest = partial rest_domA in
          elems_B |> List.concat_map @@ fun b ->
          funs_on_rest |> List.filter_map @@ fun rest_fun ->
          (* the partial function
               (a ↦ b) :: rest_fun
             is monotone if, for all pairs (a' ↦ b') in rest_fun,
             if a ≤ a' then b ≤ b' *)
          let ok (a', b') =
            if leq lA a a' then
              leq lB b b'
            else true
          in
          if List.for_all ok rest_fun
          then Some ((a, b) :: rest_fun)
          else None
      in
      partial domA

  and eq : type a . a poset -> a -> a -> bool =
    fun poset a1 a2 ->
    order
      (function Eq -> true | Lt | Gt -> false)
      poset a1 a2

  and leq : type a . a poset -> a -> a -> bool =
    fun poset a1 a2 ->
    order
      (function Lt | Eq -> true | Gt -> false)
      poset a1 a2

  and order : type a . (Ord.t -> bool) -> a poset -> a -> a -> bool =
    fun p -> function
    | Linear _ ->
      (fun i j ->
         incr Config.basic_ops;
         p (if i < j then Ord.Lt else if i > j then Ord.Gt else Ord.Eq)
      )
    | Product (lA, lB) ->
      (fun (a1, b1) (a2, b2) ->
         order p lA a1 a2 && order p lB b1 b2
      )
    | Sum (lA, lB) ->
      (fun s1 s2 -> match s1, s2 with
         | Either.Left a1, Either.Left a2 -> order p lA a1 a2
         | Either.Right b1, Either.Right b2 -> order p lB b1 b2
         | Either.Left _, Either.Right _
         | Either.Right _, Either.Left _
           -> false
      )
    | MonFun (lA, lB) ->
      (fun graph1 graph2 ->
        try
          (* Note: we assume that the function graphs are enumerated
             with the same input orders, to avoid expensive List.find
             queries to reorder the graphs. *)
          List.iter2 (fun (a, b1) (_a, b2) ->
            if Config.check_graph_order then
              assert (eq lA a _a);
            if not (order p lB b1 b2) then raise Exit
          ) graph1 graph2;
          true
        with Exit -> false
      )
end

module type Implem = sig
  type poset
  val two : poset
  val mon : poset -> poset -> poset
  val cardinal : poset -> int
end

module Test (Impl : Implem) = struct
  let two = Impl.two
  let (^->) = Impl.mon
  let check n poset =
    let c = Impl.cardinal poset in
    c = n
    || (Printf.eprintf "got %d instead of expected %n\n%!" c n; false)

  let () =
    (* nested to the left:
         twoleft{1} = two
         twoleft{n+1} = twoleft{n} -> two
       cardinal(twoleft{n}) = n+1
    *)
    assert (check 2 two);
    assert (check 3 (two ^-> two));
    assert (check 4 ((two ^-> two) ^-> two));
    assert (check 5 (((two ^-> two) ^-> two) ^-> two));
    assert (check 6 ((((two ^-> two) ^-> two) ^-> two) ^-> two));

    (* nested to the right:
         tworighy{1} = two
         tworighy{n+1} = two -> tworight{n}
       cardinal(tworighy{n}) is OEIS
         https://oeis.org/search?q=2%2C3%2C6%2C20%2C168&language=english&go=Search
       the cardinal of the free lattice with n generators,
       no closed formula known
    *)
    assert (check 2 two);
    assert (check 3 (two ^-> two));
    assert (check 6 (two ^-> two ^-> two));
    assert (check 20 (two ^-> two ^-> two ^-> two));
    assert (check 168 (two ^-> two ^-> two ^-> two ^-> two));
    assert (check 7581 (two ^-> two ^-> two ^-> two ^-> two ^-> two));

    (* in the middle *)
    assert (check 10 ((two ^-> two) ^-> (two ^-> two)));
    assert (check 35 ((two ^-> (two ^-> two)) ^-> (two ^-> two)));
    assert (check 50 ((two ^-> two) ^-> two ^-> two ^-> two));
end

module NaiveCheck : Implem = struct
  module Naive = Naive(struct
      let check_graph_order = true
      let basic_ops = ref 0
      let memoize_enumerate = true
    end)
  type poset = Lat : _ Naive.poset -> poset
  let two = Lat (Naive.Linear 2)
  let mon (Lat lA) (Lat lB) = Lat (Naive.MonFun (lA, lB))
  let cardinal (Lat lA) =
    Naive.enumerate lA |> List.length
end

module _ = Test(NaiveCheck)

let with_counter counter f =
  let count_before = !counter in
  let x = f () in
  let count_after = !counter in
  let diff = count_after - count_before in
  counter := count_before;
  x, diff

type testing_lattice =
  | Two
  | Mon of testing_lattice * testing_lattice

let lattices : testing_lattice list array =
  let limit = 10 in
  let trees = Array.make (limit + 1) [] in
  (* trees.(0) remains empty *)
  trees.(1) <- [Two];
  for n = 2 to limit do
    trees.(n) <- begin
      let splits =
          List.init (n - 1) (fun i' -> (* i' in [0; n-2] *)
            let i = i'+1 in (* i in [1; n-1] *)
            let j = n-i in (* j in [1; n-1] *)
            (i, j)
          )
        in
        splits |> List.concat_map (fun (i, j) ->
          trees.(i) |> List.concat_map @@ fun ti ->
          trees.(j) |> List.concat_map @@ fun tj ->
          [Mon(ti, tj)]
        )
      end
  done;
  trees

type 'a example = {
  lat: testing_lattice;
  card: int;
  data: 'a;
}

module NaiveImplemBench
    (Config : sig
       val memoize_enumerate : bool
     end)
= struct
  let counter = ref 0

  module Naive = Naive(struct
      let check_graph_order = false
      let basic_ops = counter
      let memoize_enumerate = Config.memoize_enumerate
    end)

  type poset = Lat : _ Naive.poset -> poset
  let two = Lat (Naive.Linear 2)
  let mon (Lat lA) (Lat lB) = Lat (Naive.MonFun (lA, lB))
  let cardinal (Lat lA) =
    Naive.enumerate lA |> List.length

  let rec of_testing_lattice = function
    | Two -> two
    | Mon(tA, tB) ->
      mon
        (of_testing_lattice tA)
        (of_testing_lattice tB)

  let example test_lat =
    let poset = of_testing_lattice test_lat in
    let (card, ticks) =
      with_counter counter (fun () -> cardinal poset)
    in
    {lat = test_lat; card; data=ticks}
end

module NaiveBench =
  NaiveImplemBench(struct let memoize_enumerate = false end)

module NaiveMemoBench =
  NaiveImplemBench(struct let memoize_enumerate = true end)

(* for toplevel use *)
let _ =
  List.init 6 (fun i -> lattices.(i))
  |> List.mapi (fun i lats ->
    (i, List.map NaiveBench.example lats)
  )
(*
[(0, []); (1, [{lat = Two; card = 2; ticks = 0}]);
 (2, [{lat = Mon (Two, Two); card = 3; ticks = 8}]);
 (3,
  [{lat = Mon (Two, Mon (Two, Two)); card = 6; ticks = 33};
   {lat = Mon (Mon (Two, Two), Two); card = 4; ticks = 50}]);
 (4,
  [{lat = Mon (Two, Mon (Two, Mon (Two, Two))); card = 20; ticks = 182};
   {lat = Mon (Two, Mon (Mon (Two, Two), Two)); card = 10; ticks = 106};
   {lat = Mon (Mon (Two, Two), Mon (Two, Two)); card = 10; ticks = 157};
   {lat = Mon (Mon (Two, Mon (Two, Two)), Two); card = 8; ticks = 597};
   {lat = Mon (Mon (Mon (Two, Two), Two), Two); card = 5; ticks = 178}]);
 (5,
  [{lat = Mon (Two, Mon (Two, Mon (Two, Mon (Two, Two)))); card = 168;
    ticks = 2726};
   {lat = Mon (Two, Mon (Two, Mon (Mon (Two, Two), Two))); card = 50;
    ticks = 645};
   {lat = Mon (Two, Mon (Mon (Two, Two), Mon (Two, Two))); card = 50;
    ticks = 694};
   {lat = Mon (Two, Mon (Mon (Two, Mon (Two, Two)), Two)); card = 35;
    ticks = 950};
   {lat = Mon (Two, Mon (Mon (Mon (Two, Two), Two), Two)); card = 15;
    ticks = 283};
   {lat = Mon (Mon (Two, Two), Mon (Two, Mon (Two, Two))); card = 50;
    ticks = 1116};
   {lat = Mon (Mon (Two, Two), Mon (Mon (Two, Two), Two)); card = 20;
    ticks = 405};
   {lat = Mon (Mon (Two, Mon (Two, Two)), Mon (Two, Two)); card = 35;
    ticks = 2802};
   {lat = Mon (Mon (Mon (Two, Two), Two), Mon (Two, Two)); card = 15;
    ticks = 527};
   {lat = Mon (Mon (Two, Mon (Two, Mon (Two, Two))), Two); card = 84;
    ticks = 93215};
   {lat = Mon (Mon (Two, Mon (Mon (Two, Two), Two)), Two); card = 16;
    ticks = 4153};
   {lat = Mon (Mon (Mon (Two, Two), Mon (Two, Two)), Two); card = 16;
    ticks = 4267};
   {lat = Mon (Mon (Mon (Two, Mon (Two, Two)), Two), Two); card = 10;
    ticks = 2331};
   {lat = Mon (Mon (Mon (Mon (Two, Two), Two), Two), Two); card = 6;
    ticks = 478}])]
*)

let _ =
  lattices.(4)
  |> fun lats ->
    let test lat = (NaiveBench.example lat,
                    NaiveMemoBench.example lat) in
    List.map test lats
(*
[({lat = Mon (Two, Mon (Two, Mon (Two, Two))); card = 20; ticks = 182},
  {lat = Mon (Two, Mon (Two, Mon (Two, Two))); card = 20; ticks = 182});
 ({lat = Mon (Two, Mon (Mon (Two, Two), Two)); card = 10; ticks = 106},
  {lat = Mon (Two, Mon (Mon (Two, Two), Two)); card = 10; ticks = 98});
 ({lat = Mon (Mon (Two, Two), Mon (Two, Two)); card = 10; ticks = 157},
  {lat = Mon (Mon (Two, Two), Mon (Two, Two)); card = 10; ticks = 141});
 ({lat = Mon (Mon (Two, Mon (Two, Two)), Two); card = 8; ticks = 597},
  {lat = Mon (Mon (Two, Mon (Two, Two)), Two); card = 8; ticks = 564});
 ({lat = Mon (Mon (Mon (Two, Two), Two), Two); card = 5; ticks = 178},
  {lat = Mon (Mon (Mon (Two, Two), Two), Two); card = 5; ticks = 128})]
*)

(* Conclusion: memoization does not bring much of a gain
   at these relatively small orders. *)

module Dist = struct
  (* A "distance" between two elements represents a zigzag of coverings:
     - In a lattice, all elements are reachable from each other by a zigzag,
       but general posets may have disconnected elements.
     - We count the number of "covering" relations, which corresponds
       to the edges of the Hasse diagram:
         x covered_by y iff
           x < y  /\  not (exists z,  x < z < y)
  *)
  type t = {
    lt: int;
    gt: int;
  }

  let conj : t -> t -> t =
    fun d1 d2 -> { lt = d1.lt + d2.lt; gt = d1.gt + d2.gt; }

  let eq = function
    | None -> false
    | Some dist ->
      dist.lt = 0 && dist.gt = 0

  let leq = function
    | None -> false
    | Some dist ->
      dist.gt = 0

  let covered_by = function
    | None -> false
    | Some dist ->
      dist.lt = 1 && dist.gt = 0
end

module Covering
    (Config : sig
       val check_graph_order : bool
       val basic_ops : int ref
     end) =
 struct
  type ('a, 'b) function_graph = ('a * 'b) list

  include Type(struct
      type ('a, 'b) monfun = ('a, 'b) function_graph
    end)

  let rec enumerate : type a . a poset -> a list =
    function
    | Linear n -> List.init n (fun i -> i)
    | Product (lA, lB) ->
      enumerate lA |> List.concat_map @@ fun a ->
      enumerate lB |> List.map @@ fun b ->
      (a, b)
    | Sum (lA, lB) ->
      List.map Either.left (enumerate lA)
      @
      List.map Either.right (enumerate lB)
    | MonFun (lA, lB) ->
      (* very naive approach: enumerate all functions,
         then filter the non-monotonous ones *)
      let domA = enumerate lA in
      let elems_B = enumerate lB in
      (* [partial dom] returns the list of all monotonous functions
         (as input-output graphs, that is, association lists)
         on a subset [dom] of [elems_A]. *)
      let rec partial = function
        | [] -> [[]]
        | a :: rest_domA ->
          let funs_on_rest = partial rest_domA in
          elems_B |> List.concat_map @@ fun b ->
          funs_on_rest |> List.filter_map @@ fun rest_fun ->
          (* the partial function
               (a ↦ b) :: rest_fun
             is monotone if, for all pairs (a' ↦ b') in rest_fun,
             if a covered by a' then b ≤ b' *)
          let ok (a', b') =
            if covered_by lA a a' then leq lB b b'
            else true
          in
          if List.for_all ok rest_fun
          then Some ((a, b) :: rest_fun)
          else None
      in
      partial domA

  and leq : type a . a poset -> a -> a -> bool =
    fun lA a1 a2 ->
    let valid dist =
      dist.Dist.gt = 0 in
    Dist.leq (order valid lA a1 a2)

  and covered_by : type a . a poset -> a -> a -> bool =
    fun lA a1 a2 ->
    let valid dist =
      dist.Dist.gt = 0 && dist.Dist.lt <= 1 in
    Dist.covered_by (order valid lA a1 a2)

  and eq : type a . a poset -> a -> a -> bool =
    fun lA a1 a2 ->
    let valid dist =
      dist.Dist.gt = 0 && dist.Dist.lt = 0 in
    Dist.eq (order valid lA a1 a2)

  and order : type a . (Dist.t -> bool) -> a poset -> a -> a -> Dist.t option =
    let (let*) = Option.bind in
    fun p ->
    let ret dist =
      if not (p dist) then None
      else Some dist
    in
    function
    | Linear _ ->
      (fun i j ->
         incr Config.basic_ops;
         ret (if i < j
              then { lt = j - i; gt = 0 }
              else { lt = 0; gt = i - j }
             )
      )
    | Product (lA, lB) ->
      (fun (a1, b1) (a2, b2) ->
         let* o1 = order p lA a1 a2 in
         let* o2 = order p lB b1 b2 in
         ret (Dist.conj o1 o2)
      )
    | Sum (lA, lB) ->
      (fun s1 s2 -> match s1, s2 with
         | Either.Left a1, Either.Left a2 -> order p lA a1 a2
         | Either.Right b1, Either.Right b2 -> order p lB b1 b2
         | Either.Left _, Either.Right _
         | Either.Right _, Either.Left _
           -> None
      )
    | MonFun (lA, lB) ->
      (fun graph1 graph2 ->
        let res = ref {Dist.lt = 0; gt = 0} in
        try
          (* Note: we assume that the function graphs are enumerated
             with the same input orders, to avoid expensive List.find
             queries to reorder the graphs. *)
          List.iter2 (fun (a, b1) (_a, b2) ->
            if Config.check_graph_order then
              assert (eq lA a _a);
            match order p lB b1 b2 with
            | None -> raise Exit
            | Some o ->
              res := Dist.conj !res o;
              if not (p !res) then raise Exit;
          ) graph1 graph2;
          Some !res
        with Exit -> None
      )

  let height : type a . a poset -> int =
    fun poset ->
      let elems = enumerate poset |> Array.of_list in
      let n = Array.length elems in
      let cover_matrix =
        Array.init n @@ fun i ->
        Printf.eprintf "covers matrix: %d/%d\n%!" (i+1) n;
        Array.init n @@ fun j ->
        covered_by poset elems.(i) elems.(j)
      in
      let height_above =
        let thunks = Array.make n (lazy 0) in
        thunks |> Array.iteri (fun i _ ->
          thunks.(i) <- lazy begin
            let h = ref 1 in
            cover_matrix.(i) |> Array.iteri (fun j i_covered_by_j ->
              if i_covered_by_j then h := max !h (1 + Lazy.force thunks.(j))
            );
            !h
          end
        );
        thunks
      in
      Array.fold_left (fun acc h ->
        max acc (Lazy.force h)
      ) 0 height_above

end

module CoveringCheck : Implem = struct
  module Covering = Covering(struct
      let check_graph_order = true
      let basic_ops = ref 0
    end)
  type poset = Lat : _ Covering.poset -> poset
  let two = Lat (Covering.Linear 2)
  let mon (Lat lA) (Lat lB) = Lat (Covering.MonFun (lA, lB))
  let cardinal (Lat lA) =
    Covering.enumerate lA |> List.length
end

module _ = Test(CoveringCheck)

module CoveringBench = struct
  let counter = ref 0

  module Covering = Covering(struct
      let check_graph_order = false
      let basic_ops = counter
    end)

  type poset = Lat : _ Covering.poset -> poset
  let two = Lat (Covering.Linear 2)
  let mon (Lat lA) (Lat lB) = Lat (Covering.MonFun (lA, lB))
  let cardinal (Lat lA) =
    Covering.enumerate lA |> List.length
  let height (Lat lA) =
    Covering.height lA

  let rec of_testing_lattice = function
    | Two -> two
    | Mon(tA, tB) ->
      mon
        (of_testing_lattice tA)
        (of_testing_lattice tB)

  let example test_lat =
    let poset = of_testing_lattice test_lat in
    let (card, ticks) =
      with_counter counter (fun () -> cardinal poset)
    in
    let height = height poset in
    {lat = test_lat; card; data = (ticks, ("height", height))}
end


let _ =
  let test lat = ((* NaiveBench.example lat, *)
                  CoveringBench.example lat) in
  let i = 5 in
  (i, List.map test lattices.(i))
(*
(5,
 [({lat = Mon (Two, Mon (Two, Mon (Two, Mon (Two, Two)))); card = 168;
    ticks = 2726},
   {lat = Mon (Two, Mon (Two, Mon (Two, Mon (Two, Two)))); card = 168;
    ticks = 2726});
  ({lat = Mon (Two, Mon (Two, Mon (Mon (Two, Two), Two))); card = 50;
    ticks = 645},
   {lat = Mon (Two, Mon (Two, Mon (Mon (Two, Two), Two))); card = 50;
    ticks = 641});
  ({lat = Mon (Two, Mon (Mon (Two, Two), Mon (Two, Two))); card = 50;
    ticks = 694},
   {lat = Mon (Two, Mon (Mon (Two, Two), Mon (Two, Two))); card = 50;
    ticks = 674});
  ({lat = Mon (Two, Mon (Mon (Two, Mon (Two, Two)), Two)); card = 35;
    ticks = 950},
   {lat = Mon (Two, Mon (Mon (Two, Mon (Two, Two)), Two)); card = 35;
    ticks = 852});
  ({lat = Mon (Two, Mon (Mon (Mon (Two, Two), Two), Two)); card = 15;
    ticks = 283},
   {lat = Mon (Two, Mon (Mon (Mon (Two, Two), Two), Two)); card = 15;
    ticks = 256});
  ({lat = Mon (Mon (Two, Two), Mon (Two, Mon (Two, Two))); card = 50;
    ticks = 1116},
   {lat = Mon (Mon (Two, Two), Mon (Two, Mon (Two, Two))); card = 50;
    ticks = 916});
  ({lat = Mon (Mon (Two, Two), Mon (Mon (Two, Two), Two)); card = 20;
    ticks = 405},
   {lat = Mon (Mon (Two, Two), Mon (Mon (Two, Two), Two)); card = 20;
    ticks = 341});
  ({lat = Mon (Mon (Two, Mon (Two, Two)), Mon (Two, Two)); card = 35;
    ticks = 2802},
   {lat = Mon (Mon (Two, Mon (Two, Two)), Mon (Two, Two)); card = 35;
    ticks = 2190});
  ({lat = Mon (Mon (Mon (Two, Two), Two), Mon (Two, Two)); card = 15;
    ticks = 527},
   {lat = Mon (Mon (Mon (Two, Two), Two), Mon (Two, Two)); card = 15;
    ticks = 418});
  ({lat = Mon (Mon (Two, Mon (Two, Mon (Two, Two))), Two); card = 84;
    ticks = 93215},
   {lat = Mon (Mon (Two, Mon (Two, Mon (Two, Two))), Two); card = 84;
    ticks = 65711});
  ({lat = Mon (Mon (Two, Mon (Mon (Two, Two), Two)), Two); card = 16;
    ticks = 4153},
   {lat = Mon (Mon (Two, Mon (Mon (Two, Two), Two)), Two); card = 16;
    ticks = 3085});
  ...])
*)

(* Conclusion: the 'covering' criterion has a relatively small benefit
   in terms of saved comparisons.
   My hypothesis as to why this is the case is as follows: some of the
   redudant comparisons allowed by the naive implementation are in
   fact useful as they filter out non-monotous graphs earlier.

   On our our most costly example, we still go from
     93_215 to 65_711
   comparisons, which makes it a worthwhile optimization.
*)


(* for ocamlopt-compiled use *)
let () =
  let i = 6 in
  let rec test_lat = function 1 -> Two | n -> Mon(Two, test_lat (n-1)) in
  let example = CoveringBench.example (test_lat i) in
  Printf.printf "Result for n=%d: card=%d, height=%d\n%!"
    i example.card (example.data |> snd |> snd)
